#!/bin/bash

# test
/usr/bin/CloudflareST -n 1000 -dn 32 -dt 10 -tl 200 -tlr 0 -sl 5 -p 32 -f ip.txt -url https://cf-speedtest.acfun.win/200mb.test -o ./results.csv
# /usr/bin/CloudflareST -n 1000 -dn 32 -dt 10 -tl 300 -tlr 0 -sl 1 -p 32 -f ipv6.txt -url https://cf-speedtest.acfun.win/200mb.test -o ./results.csv

# update results
/usr/bin/git add ./results.csv
/usr/bin/git commit -m "update results"
/usr/bin/git push origin main
